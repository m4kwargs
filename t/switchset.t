!C This file is part of M4KWARGS
!C Copyright (C) 2015 Sergey Poznyakoff
!C
!C M4KWARGS is free software; you can redistribute it and/or modify
!C it under the terms of the GNU General Public License as published by
!C the Free Software Foundation; either version 3, or (at your option)
!C any later version.
!C
!C M4KWARGS is distributed in the hope that it will be useful,
!C but WITHOUT ANY WARRANTY; without even the implied warranty of
!C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!C GNU General Public License for more details.
!C
!C You should have received a copy of the GNU General Public License
!C along with M4KWARGS.  If not, see <http://www.gnu.org/licenses/>.
!IN
# BEGIN
KWARGS_WITH(
`KWARGS_SWITCH(`foo',`foo is set',
`bar',`bar is set',
`baz',`baz is set',
`none')')
KWARGS_WITH(
`KWARGS_SWITCH(`foo',`foo is set',
`bar',`bar is set',
`baz',`baz is set',
`none')',
`foo=1',`baz=2')
KWARGS_WITH(
`KWARGS_SWITCH(`foo',`foo is set',
`bar',`bar is set',
`baz',`baz is set',
`none')',
`bar=1',`foo=2',`baz=3')
KWARGS_WITH(
`KWARGS_SWITCH(`foo',`foo is set',
`bar',`bar is set',
`baz',`baz is set',
`none')',
`foo=1',`bar=2',`baz=3')
KWARGS_WITH(
`KWARGS_SWITCH(`foo',`foo is set',
`bar',`bar is set',
`baz',`baz is set',
`none')',
`baz=1')
KWARGS_WITH(
`KWARGS_SWITCH(`foo',`foo is set',
`bar',`bar is set',
`baz',`baz is set',
`other')',
`quux=1')
# END
!OUT
# BEGIN
none
FOO is set
FOO is set
FOO is set
BAZ is set
other
# END
!END
