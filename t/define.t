!C This file is part of M4KWARGS
!C Copyright (C) 2018 Sergey Poznyakoff
!C
!C M4KWARGS is free software; you can redistribute it and/or modify
!C it under the terms of the GNU General Public License as published by
!C the Free Software Foundation; either version 3, or (at your option)
!C any later version.
!C
!C M4KWARGS is distributed in the hope that it will be useful,
!C but WITHOUT ANY WARRANTY; without even the implied warranty of
!C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!C GNU General Public License for more details.
!C
!C You should have received a copy of the GNU General Public License
!C along with M4KWARGS.  If not, see <http://www.gnu.org/licenses/>.
!IN
# DEFINE
KWARGS_DEFINE(`FOO',
`argv=KW_ARGV
argv[1]=KW_ARGV(1)
argc=KW_ARGC
arg@=KW_ARGQ
y=KWARG_VALUE(y)
dump:
KWARGS_DUMP()')m4_dnl
# EXPAND
FOO(a,`b,x',c,x=4,y=`5,67',a=5)
# END
!OUT
# DEFINE
# EXPAND
argv=a,b,x,c
argv[1]=a
argc=3
arg@=`a',`b,x',`c'
y=5,67
dump:
x=4
y=5,67
a=5
# END
!END
