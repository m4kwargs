#! /bin/sh
# This file is part of M4KWARGS test suite.
# Copyright (C) 2015-2018 Sergey Poznyakoff
#
# M4KWARGS is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# M4KWARGS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with M4KWARGS.  If not, see <http://www.gnu.org/licenses/>.

# runtest [-I DIR] BASENAME

M4KWARGS=
while getopts "I:D:" OPT
do
    case $OPT in
	I|D) M4KWARGS="$M4KWARGS -$OPT $OPTARG";;
	*) exit 1
    esac
done

shift $(($OPTIND - 1))

ntest=$#
nrun=0
fail=0
for t in $*
do
    case $t in
	*.t)	t=${t%%.t};;
    esac
    
    if [ -r $t.t ]; then
	if sed '/^!C/d;/^!IN/d;/^!OUT/,$d' $t.t | m4 $M4KWARGS setup.m4 - >$t.out 2>$t.err; then
	    nrun=$(($nrun + 1))
	    if [ -s $t.err ]; then
		echo "$0: $t: FAIL (error output)"
		fail=$(($fail + 1))
	    elif sed '1,/^!OUT/d;/^!END/,$d' $t.t | diff -u - $t.out > $t.diff; then
		echo "$0: $t: PASS"
		rm $t.out $t.err $t.diff
	    else
		echo "$0: $t: FAIL (diff)"
		fail=$(($fail + 1))
	    fi
	else
	    rc=$?
	    if [ -s $t.err ]; then
		echo "$0: $t: FAIL (code $rc; error output)"
	    else
	        echo "$0: $t: FAIL (code $rc)"
	    fi
  	    fail=$(($fail + 1))
	fi
    else
	echo >&2 "$0: testcase $t.t not found"
    fi
done

echo "$0: Run $nrun out of $ntest tests"
if [ $fail -ne 0 ]; then
    echo "$0: Failures: $fail"
fi
if [ $ntest != $nrun ]; then
    exit 2
elif [ $fail -ne 0 ]; then
    exit 1
fi
exit 0
