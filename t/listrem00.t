!C This file is part of M4KWARGS
!C Copyright (C) 2015 Sergey Poznyakoff
!C
!C M4KWARGS is free software; you can redistribute it and/or modify
!C it under the terms of the GNU General Public License as published by
!C the Free Software Foundation; either version 3, or (at your option)
!C any later version.
!C
!C M4KWARGS is distributed in the hope that it will be useful,
!C but WITHOUT ANY WARRANTY; without even the implied warranty of
!C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!C GNU General Public License for more details.
!C
!C You should have received a copy of the GNU General Public License
!C along with M4KWARGS.  If not, see <http://www.gnu.org/licenses/>.
!IN
m4_define(`list',``foo',`bar',`baz'')m4_dnl
# BEGIN
m4_kwargs_list_remove_(`foo',list)
m4_kwargs_list_remove_(`bar',list)
m4_kwargs_list_remove_(`baz',list)
# END
!OUT
# BEGIN
,bar,baz
,foo,baz
,foo,bar
# END
!END
