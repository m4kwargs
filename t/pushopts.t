!C This file is part of M4KWARGS
!C Copyright (C) 2015 Sergey Poznyakoff
!C
!C M4KWARGS is free software; you can redistribute it and/or modify
!C it under the terms of the GNU General Public License as published by
!C the Free Software Foundation; either version 3, or (at your option)
!C any later version.
!C
!C M4KWARGS is distributed in the hope that it will be useful,
!C but WITHOUT ANY WARRANTY; without even the implied warranty of
!C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!C GNU General Public License for more details.
!C
!C You should have received a copy of the GNU General Public License
!C along with M4KWARGS.  If not, see <http://www.gnu.org/licenses/>.
!IN
# BEGIN
KWARGS_ASGN(`foo=qux',`bar=baz',`baz=foo,bar')m4_dnl
# DUMP
KWARGS_DUMP()
# OVERWRITE
KWARGS_PUSH(`foo=1',`baz=2')m4_dnl
# DUMP
KWARGS_DUMP()
# STACK foo
m4_defn(`--foo')
m4_popdef(`--foo')m4_dnl
m4_defn(`--foo')
# STACK bar
m4_defn(`--bar')
m4_popdef(`--bar')m4_dnl
m4_defn(`--bar')
# STACK baz
m4_defn(`--baz')
m4_popdef(`--baz')m4_dnl
m4_defn(`--baz')
# END
!OUT
# BEGIN
# DUMP
foo=qux
bar=baz
baz=foo,bar
# OVERWRITE
# DUMP
foo=1
baz=2
# STACK foo
1
qux
# STACK bar
baz

# STACK baz
2
foo,bar
# END
!END
