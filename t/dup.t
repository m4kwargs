!C This file is part of M4KWARGS
!C Copyright (C) 2015 Sergey Poznyakoff
!C
!C M4KWARGS is free software; you can redistribute it and/or modify
!C it under the terms of the GNU General Public License as published by
!C the Free Software Foundation; either version 3, or (at your option)
!C any later version.
!C
!C M4KWARGS is distributed in the hope that it will be useful,
!C but WITHOUT ANY WARRANTY; without even the implied warranty of
!C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!C GNU General Public License for more details.
!C
!C You should have received a copy of the GNU General Public License
!C along with M4KWARGS.  If not, see <http://www.gnu.org/licenses/>.
!IN
# BEGIN
KWARGS_ASGN(alpha=a,charlie=`TEX,T',bravo=bar)m4_dnl
_m4_kwargs_list
KWARGS_DUMP
# DUP
KWARGS_DUP()m4_dnl
_m4_kwargs_list
KWARGS_DUMP
# ASGN
KWARGS_ASGN(alpha=A,delta=5)m4_dnl
_m4_kwargs_list
KWARGS_DUMP
# POP
KWARGS_POP()m4_dnl
_m4_kwargs_list
KWARGS_DUMP
# END
!OUT
# BEGIN
alpha,charlie,bravo
alpha=a
charlie=TEX,T
bravo=BAR
# DUP
alpha,charlie,bravo
alpha=a
charlie=TEX,T
bravo=BAR
# ASGN
alpha,charlie,bravo,delta
alpha=A
charlie=TEX,T
bravo=BAR
delta=5
# POP
alpha,charlie,bravo
alpha=a
charlie=TEX,T
bravo=BAR
# END
!END
