!C This file is part of M4KWARGS
!C Copyright (C) 2015 Sergey Poznyakoff
!C
!C M4KWARGS is free software; you can redistribute it and/or modify
!C it under the terms of the GNU General Public License as published by
!C the Free Software Foundation; either version 3, or (at your option)
!C any later version.
!C
!C M4KWARGS is distributed in the hope that it will be useful,
!C but WITHOUT ANY WARRANTY; without even the implied warranty of
!C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!C GNU General Public License for more details.
!C
!C You should have received a copy of the GNU General Public License
!C along with M4KWARGS.  If not, see <http://www.gnu.org/licenses/>.
!IN
m4_divert(-1)
m4_define(`_m4_kwargs_list',``foo',`bar',`baz'')m4_dnl
m4_pushdef(`--foo',`1')
m4_pushdef(`--bar',`Bar')
m4_pushdef(`--baz',`Baz qux')
m4_define(`printarg',`// `$1'')
m4_divert(0)m4_dnl
// BEGIN
KWARGS_DUMP()
KWARGS_DUMP(,`printarg')
// END
!OUT
// BEGIN
foo=1
bar=Bar
baz=Baz qux
// foo=1
// bar=Bar
// baz=Baz qux
// END
!END
