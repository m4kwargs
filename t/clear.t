!C This file is part of M4KWARGS
!C Copyright (C) 2015 Sergey Poznyakoff
!C
!C M4KWARGS is free software; you can redistribute it and/or modify
!C it under the terms of the GNU General Public License as published by
!C the Free Software Foundation; either version 3, or (at your option)
!C any later version.
!C
!C M4KWARGS is distributed in the hope that it will be useful,
!C but WITHOUT ANY WARRANTY; without even the implied warranty of
!C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!C GNU General Public License for more details.
!C
!C You should have received a copy of the GNU General Public License
!C along with M4KWARGS.  If not, see <http://www.gnu.org/licenses/>.
!IN
# BEGIN
KWARG_SET(`foo',314)
KWARG_SET(`bar',`q1')
KWARG_SET(`baz-bar',`shmeer')
# DUMP
KWARGS_DUMP()
# CLEAR bar
KWARG_CLR(`bar')m4_dnl
KWARGS_DUMP()
# CLEAR bar 2
KWARG_CLR(`bar')m4_dnl
KWARGS_DUMP()
# CLEAR baz-bar
KWARG_CLR(`baz-bar')m4_dnl
KWARGS_DUMP()
# CLEAR foo
KWARG_CLR(`foo')m4_dnl
KWARGS_DUMP()
# END
!OUT
# BEGIN



# DUMP
foo=314
bar=q1
baz-bar=shmeer
# CLEAR bar
foo=314
baz-bar=shmeer
# CLEAR bar 2
foo=314
baz-bar=shmeer
# CLEAR baz-bar
foo=314
# CLEAR foo

# END
!END
