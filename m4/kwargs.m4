dnl This file is part of M4KWARGS
dnl Copyright (C) 2011-2018 Sergey Poznyakoff
dnl
dnl M4KWARGS is free software; you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 3, or (at your option)
dnl any later version.
dnl
dnl M4KWARGS is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl
dnl You should have received a copy of the GNU General Public License
dnl along with M4KWARGS.  If not, see <http://www.gnu.org/licenses/>.
dnl
dnl The 'bracketquotes' comment line will be replaced by make install
dnl with the appropriate changequote statement, if required.
dnl bracketquotes
pushdef(`__diversion__',divnum)divert(-1)

dnl Determine whether the patsubst builtin is available and whether it
dnl takes an extended regular expression as its second argument.
define(`m4_regex_extended',
  ifdef(`patsubst',
    `ifelse(patsubst(`ab',`^(.)b',`\1'),`a',1,0)',
    `errprint(`patsubst is required, but not present')m4exit(1)'))

include(prefix.m4)
m4_include(composite.m4)

# m4_argc(...)
# ------------
# Expands to the number of arguments supplied.
#
m4_define(`m4_argc',`$#')

# m4_ifempty(LIST, IF-TRUE [, IF-FALSE])
# --------------------------------------
# Expands to IF-TRUE if LIST is not empty, otherwise to IF-FALSE.
#
m4_define(`m4_ifempty', `m4_ifelse(`$1',`',`$2',`$3')')

# m4_quote_names(LIST)
# --------------------
# Produce a quoted comma-delimited list of doubly-quoted names from LIST.
# The result is suitable for use as second argument to m4_foreach, so that
# none of its elements is expanded while processing.
m4_define(`m4_quote_names', `m4_quote(m4_dquote(m4_dquote_elt($@)))')

# m4_kwargs_list_if_member(LIST, ELT, IF-TRUE [, IF-FALSE])
# ---------------------------------------------------------
# Expands to IF-TRUE if ELT is contained in the comma-delimited LIST of
# elements.  Otherwise, expands to IF-FALSE.
m4_define(`m4_kwargs_list_if_member',`_m4_kwargs_list_if_member__(`$3',`$4',`$2',$1)')
# _m4_kwargs_list_if_member(IF-TRUE, IF-FALSE, ELT, ...)
# ------------------------------------------------------
# Auxiliary function for m4_kwargs_list_if_member.  Recursively scans the list
# of arguments beginning with $4 and stops when ELT is found, or end of
# the list is reached.  Expands to IF-TRUE or IF-FALSE, depending on
# whether ELT is found.
#
m4_define(`_m4_kwargs_list_if_member__',
`m4_ifelse(`$4',`',`$2',
`$3',`$4',`$1',`_m4_kwargs_list_if_member__(`$1',`$2',`$3',m4_shift(m4_shift(m4_shift(m4_shift($@)))))')')

# m4_kwargs_list_remove(LIST, ELT)
# --------------------------------
# Removes ELT from the comma-delimited LIST.
#
m4_define(`m4_kwargs_list_remove',
`m4_pushdef(`__L__',m4_dquote($1))m4_dnl
m4_popdef(`$1')m4_dnl
m4_pushdef(`$1',m4_quote(m4_shift(m4_dquote_elt(m4_kwargs_list_remove_(`$2',__L__)))))m4_dnl
m4_popdef(`__L__')')

# m4_kwargs_list_remove_(elt, ...)
m4_define(`m4_kwargs_list_remove_',
`m4_ifelse(`$#',1,`',
`$#',2,`m4_ifelse(`$1',`$2',,`,m4_quote(`$2')')',
`m4_ifelse(`$1',`$2',,`,m4_quote(`$2')')m4_kwargs_list_remove_(`$1',m4_shift(m4_shift($@)))')')

# m4_kwargs_mangle_name(NAME)
# ---------------------------
# Converts NAME to a keyword name by prefixing it with double-dash.
m4_define(`m4_kwargs_mangle_name',`--`$1'')

# _m4_kwargs_list - a list of defined keyword names
# ---------------------------------------------
#
m4_define(`_m4_kwargs_list')

# KWARG_SET(NAME [, VALUE])
# -------------------------
# Sets argument NAME to VALUE
m4_define(`KWARG_SET',
`m4_ifelse(`$1',`',,
`m4_pushdef(`__opt_name__',m4_dquote(m4_kwargs_mangle_name(`$1')))m4_dnl
m4_kwargs_list_if_member(`_m4_kwargs_list',`$1',`m4_popdef(__opt_name__)',
`m4_define(`_m4_kwargs_list',m4_ifempty(m4_quote(_m4_kwargs_list),
m4_dquote(m4_dquote(`$1')),`m4_quote(m4_dquote(_m4_kwargs_list), m4_dquote(`$1'))'))')m4_dnl
m4_pushdef(__opt_name__,`$2')m4_dnl
m4_popdef(`__opt_name__')')')

# KWARG_CLR(NAME)
# ---------------
# Clears the keyword argument NAME.
m4_define(`KWARG_CLR',
`m4_kwargs_list_if_member(`_m4_kwargs_list',`$1',
`m4_popdef(m4_kwargs_mangle_name(`$1'))m4_dnl
m4_kwargs_list_remove(`_m4_kwargs_list',`$1')')')

# KWARG_ASGN(NAME[=VAL])
# ----------------------
# Assigns VAL to NAME.  If VAL is absent, equivalent to KWARG_SET(NAME).
m4_define(`KWARG_ASGN',
`KWARG_SET(m4_ifelse(m4_index(`$1',`='),-1,``$1'',`m4_patsubst(`$1',m4_ifelse(m4_regex_extended,0,``\([^=]*\)=\(.*\)'',``([^=]*)=(.*)''),``\1',`\2'')'))')

# KWARG_UNASGN(NAME[=VAL])
# ------------------------
# Unassign argument.  Optional VAL is ignored.
m4_define(`KWARG_UNASGN',
`KWARG_CLR(m4_patsubst(`$1',m4_ifelse(m4_regex_extended,0,``\([^=]*\)=\(.*\)'',``([^=]*)=(.*)''),``\1''))')

# KWARG_IFSET(NAME,IF-SET,IF-NOT-SET)
# -----------------------------------
# If the argument NAME is set, expands to IF-SET, otherwise expands
# to IF-NOT-SET.
m4_define(`KWARG_IFSET',
`m4_kwargs_list_if_member(`_m4_kwargs_list',`$1',`$2',`$3')')

# KWARG_VALUE(NAME[,DEFAULT])
# ---------------------------
# Expands to the value of argument NAME, if it is defined, or to DEFAULT
# otherwise.
m4_define(`KWARG_VALUE',
`m4_kwargs_list_if_member(`_m4_kwargs_list',`$1',`m4_defn(m4_kwargs_mangle_name(`$1'))',`$2')')

# KWARGS_SWITCH_VALUE(NAME, VAL1, CASE1, VAL2, CASE2,..., OTHERWISE)
# ------------------------------------------------------------------
# If keyword argument NAME has value VAL1, expands to CASE1. Otherwise,
# if NAME has value VAL2, expands to CASE2, etc.
# If no matching value is listed, expands to OTHERWISE.
m4_define(`KWARGS_SWITCH_VALUE',
`m4_kwargs_switch_val(KWARG_VALUE(`$1'),m4_shift($@))')
m4_define(`m4_kwargs_switch_val',
`m4_ifelse(`$#',2,`$2',
`$#',1,`',
`$1',`$2',`$3',`$0(`$1',m4_shift(m4_shift(m4_shift($@))))')')')

# KWARGS_SWITCH(NAME1,IF-SET1,NAME2,IF-SET2,...,IF-NOT-SET)
# ---------------------------------------------------------
# If NAME1 is set, expand to IF-SET1.  Otherwise, if NAME2 is set, expand to
# SET2. Continue the process for all name-if-set pairs within `...'.  If none
# of the arguments is set, expand to IF-NOT-SET.
m4_define(`KWARGS_SWITCH',
`m4_ifelse(`$4',,`KWARG_IFSET($@)',m4_dnl
`$3',,`KWARG_IFSET($@)',m4_dnl
`KWARG_IFSET(`$1',`$2',`KWARGS_SWITCH(m4_shift(m4_shift($@)))')')')

# KWARGS_ASGN(ARGS...)
# --------------------
# Assign arguments.
# ARGS is a comma-separated list of keyword[=value] pairs.
m4_define(`KWARGS_ASGN',
`m4_foreach(`_GR_Arg', m4_quote_names($@),
 `KWARG_ASGN(m4_quote(_GR_Arg))')')

# KWARGS_UNASGN(ARGS...])
# -----------------------
# Unassign arguments.
m4_define(`KWARGS_UNASGN',
`m4_foreach(`_GR_Arg', m4_quote_names($@),
 `KWARG_UNASGN(m4_quote(_GR_Arg))')')

# KWARGS_PUSH([ARGS...])
# ----------------------
# Push current keyword arguments on stack, clear the keyword argument table
# and redefine arguments from ARGS.
m4_define(`KWARGS_PUSH',
`m4_pushdef(`_m4_kwargs_list')m4_dnl
KWARGS_ASGN($@)')

# KWARGS_DUP()
# ------------
# Push current keyword arguments on stack and retain their definitions.
# This can be used to augment keyword arguments with new settings. To
# restore previous arguments, use KWARGS_POP. Normal usage pattern is:
#   KWARGS_PUSH()
#   KWARGS_ASGN($@)
#   ...
#   KWARGS_POP
m4_define(`KWARGS_DUP',
`KWARGS_FOREACH(`param',
  `m4_pushdef(m4_kwargs_mangle_name(param),
    m4_defn(m4_kwargs_mangle_name(param)))')m4_dnl
m4_pushdef(`_m4_kwargs_list',m4_quote(_m4_kwargs_list))')

# KWARGS_POP()
# ------------
m4_define(`KWARGS_POP',
`KWARGS_FOREACH(`opt',`m4_popdef(m4_kwargs_mangle_name(opt))')m4_dnl
m4_popdef(`_m4_kwargs_list')')

# KWARGS_FOREACH(ITERATOR, TEXT)
# ------------------------------
# Takes the name in ITERATOR, which must be a valid identifier, and
# successively assigns it each keyword name, in the same order they were
# created.  For each assignment to ITERATOR, TEXT is appended to the expansion
# of KWARGS_FOREACH.  TEXT may refer to ITERATOR.  Any definition of ITERATOR
# prior to this invocation is restored.
#
m4_define(`KWARGS_FOREACH',
  `m4_foreach(`$1',m4_quote_names(_m4_kwargs_list),`$2')')

# KWARGS_DUMP([DELIM],[EXP])
# ---------------------
# For each defined keyword argument, expand to
#   `KW=VAL'DELIM
# If EXP is supplied, expand to
#   EXP(`KW=VAL')DELIM
# instead.
# Default DELIM is newline.
m4_define(`KWARGS_DUMP',
 `_$0(`m4_ifelse(`$1',,`
',`$1')',`m4_ifelse(`$2',`',`m4_quote',`$2')',_m4_kwargs_list)')

m4_define(`_KWARGS_DUMP',
 `m4_ifelse(`$3',,,
  `m4_quote($2(`$3'=KWARG_VALUE(`$3')))_$0(`$1',`$2',m4_shift(m4_shift(m4_shift($@))))')')

# _KWARGS_DUMP(DELIM,EXP,ARGS...)
m4_define(`__KWARGS_DUMP',
 `m4_ifelse(`$3',,,
   `$1_KWARGS_DUMP($@)')')

m4_define(`KWARGS_LIST',`KWARGS_DUMP(`,')')

m4_define(`KW_ARGV',
`m4_ifelse($#,0,`KW_ARGS',`m4_argn($1,KW_ARGS)')')

m4_define(`KW_ARGC',
`m4_argc(KW_ARGS)')
m4_define(`KW_ARGQ',
`m4_dquote_elt(KW_ARGS)')

# KWARGS_APPLY(MACRO,[ARGS...])
# -----------------------------
# Split ARGS into positional and keyword arguments and expand to
# MACRO(PARGS), where PARGS are positional arguments.
#
m4_define(`KWARGS_APPLY',
`KWARGS_PUSH()m4_dnl
m4_pushdef(`KW_ARGS',m4_quote(KWARGS_PARSE(m4_shift($@))))m4_dnl
$1(KW_ARGS)`'m4_dnl
KWARGS_POP()m4_dnl
m4_popdef(`KW_ARGS')')

# KWARGS_WITH(CODE,ARGS...)
# ------------------------
# Expand to CODE with positional and keyword arguments from ARGS...
m4_define(`KWARGS_WITH',
`m4_pushdef(`__call__',`$1')KWARGS_APPLY(`__call__',m4_shift($@))m4_popdef(`__call__')')

m4_define(`m4_kwargs_parse',
`m4_ifelse(`$#',`0',`',m4_is_kwarg(`$1'),`1',`KWARGS_ASGN($@)',`,``$1''`'_m4_kwargs_parse($@)')')
m4_define(`_m4_kwargs_parse',`m4_ifelse(`$#',1,`',`m4_kwargs_parse(m4_shift($@))')')

# KWARGS_PARSE(ARGS...)
# ---------------------
# Split ARGS in positional and keyword arguments. Define the latter, expand
# to the former.
m4_define(`KWARGS_PARSE',`m4_shift(m4_kwargs_parse($@))')

# m4_is_kwarg(ARG)
# ----------------
# Expand to 1 if ARG looks like a valid keyword argument, and to 0 otherwise.
m4_define(`m4_is_kwarg',
`m4_ifelse(`$1',`1',`0',
m4_quote(m4_patsubst(`$1',`^[a-zA-Z_-][a-zA-Z0-9_-]*=.*',`1')),`1',`1',`0')')

# KWARGS_DEFINE(NAME,TEXT)
# ------------------------
# Defines NAME to expand to TEXT. Upon expansion, the argument list
# is split into positional arguments and keyword arguments. Instead of
# using the familiar $X notation, TEXT should refer to positional arguments
# as described in the table below:
#
#   $1        KW_ARGV(1)
#   $2        KW_ARGV(2)
#   ...
#   $*        KW_ARGV
#   $@        KW_ARGQ
#   $#        KW_ARGC
m4_define(`KWARGS_DEFINE',`m4_define(`$1',`KWARGS_WITH'(m4_dquote(`$2'),m4_quote_elt($`'@)))')

m4_divert(__diversion__)m4_popdef(`__diversion__')m4_dnl
