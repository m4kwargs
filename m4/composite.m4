# Composite macros from GNU M4 Manual

# m4_forloop (ITERATOR, START, END, TEXT)
# ---------------------------------------
#     Takes the name in ITERATOR, which must be a valid macro name, and
#     successively assigns it each integer value from START to END,
#     inclusive.  For each assignment to ITERATOR, append TEXT to the
#     expansion of the `m4_forloop'.  TEXT may refer to ITERATOR.  Any
#     definition of ITERATOR prior to this invocation is restored.
# Improved version:
#     * works even if ITERATOR is not a strict macro name
#     * performs sanity check that START is larger than END
#     * allows complex numerical expressions in END and START
m4_define(`m4_forloop', `m4_ifelse(m4_eval(`($2) <= ($3)'), `1',
  `m4_pushdef(`$1')_$0(`$1', m4_eval(`$2'),
    m4_eval(`$3'), `$4')m4_popdef(`$1')')')
m4_define(`_m4_forloop',
  `m4_define(`$1', `$2')$4`'m4_ifelse(`$2', `$3', `',
    `$0(`$1', m4_incr(`$2'), `$3', `$4')')')

# m4_foreach (ITERATOR, QUOTE-LIST, TEXT)
# ---------------------------------------
#     Takes the name in ITERATOR, which must be a valid macro name, and
#     successively assigns it each value from QUOTE-LIST. QUOTE-LIST is
#     a comma-separated list of elements contained in a quoted string.
#     For each assignment to ITERATOR, append TEXT to the overall
#     expansion.  TEXT may refer to ITERATOR.  Any definition of
#     ITERATOR prior to this invocation is restored.
m4_define(`m4_foreach', `m4_ifelse(`$2', `', `',
  `m4_pushdef(`$1')_$0(`$1', `$3', `', $2)m4_popdef(`$1')')')
m4_define(`_m4_foreach', `m4_ifelse(`$#', `3', `',
  `m4_define(`$1', `$4')$2`'$0(`$1', `$2',
       m4_shift(m4_shift(m4_shift($@))))')')

# m4_quote(ARGS) - convert ARGS to single-quoted string
# -----------------------------------------------------
m4_define(`m4_quote', `m4_ifelse(`$*', `', `', ``$*'')')

# m4_dquote(ARGS) - convert ARGS to quoted list of quoted strings
# ---------------------------------------------------------------
m4_define(`m4_dquote', `m4_ifelse(`$*', `', `', ``$@'')')

# m4_quote_elt(ARGS) - convert ARGS to list of quoted strings
# -----------------------------------------------------------
m4_define(`m4_quote_elt', `m4_ifelse(`$*', `', `', `$#', `1', ``$1'',
                             ``$1',$0(m4_shift($@))')')
# m4_dquote_elt(ARGS) - convert ARGS to list of double-quoted strings
# -------------------------------------------------------------------
m4_define(`m4_dquote_elt', `m4_ifelse(`$*', `', `', `$#', `1', ```$1''',
                             ```$1'',$0(m4_shift($@))')')

# m4_argn (N, ...)
# ----------------
#     Expands to argument N out of the remaining arguments.  N must be a
#     positive number.  Usually invoked as `m4_argn(`N',$@)'.
m4_define(`m4_argn', `m4_ifelse(`$1', 1, ``$2'',
 `m4_argn(m4_decr(`$1'), m4_shift(m4_shift($@)))')')


