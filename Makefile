PROJECT   = m4kwargs
VERSION   = 1.0
DISTDIR   = $(PROJECT)-$(VERSION)
DISTFILES = Makefile README COPYING
SUBDIRS   = m4 t
PREFIX    = /usr/local

all: help

help:
	@echo "Available targets are:"
	@echo ""
	@echo make check
	@echo "   Run testsuite"
	@echo ""
	@echo make install [DESTDIR=DIR] [PREFIX=DIR] [M4DIR=DIR] [BRACKETQUOTES=1]
	@echo '   Install package. By default it is installed to $$(DESTDIR)$$(PREFIX)/share/$$(M4DIR),'
	@echo "   where:"
	@echo "      \$$(DESTDIR)   is empty"
	@echo "      \$$(PREFIX)    is $(PREFIX)"
	@echo "      \$$(M4DIR)     is $(PROJECT)"
	@echo "   Any of these variables can be overridden from the command line."
	@echo ""
	@echo "   Non-existent directories will be created."
	@echo ""
	@echo "   If BRACKETQUOTES is set, traditional quotes (\`') will be changed to []."
	@echo ""
	@echo make dist
	@echo "   Create distribution tarball."
	@echo ""
	@echo make help
	@echo "   Produce this help output."
	@echo ""
	@echo "Please refer to the README file for details."
	@echo ""

install:
	for dir in $(SUBDIRS); do \
	    $(MAKE) -C $$dir install PREFIX=$(PREFIX); \
	done

distdir:
	test -d $(DISTDIR) || mkdir -p $(DISTDIR)
	cp $(DISTFILES) $(DISTDIR)
	for dir in $(SUBDIRS); do \
	    $(MAKE) -C $$dir distdir DISTDIR=../$(DISTDIR)/$$dir;\
        done

dist: distdir
	tar zcf $(DISTDIR).tar.gz $(DISTDIR)
	rm -rf $(DISTDIR)

distcheck: dist
	tar xfz $(DISTDIR).tar.gz
	if $(MAKE) -C $(DISTDIR) $(DISTCHECKFLAGS) check; then \
	  echo "$(DISTDIR).tar.gz ready for distribution"; \
	  rm -rf $(DISTDIR); \
        else \
          exit 2; \
	fi

check:
	$(MAKE) -C t check

